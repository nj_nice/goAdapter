package contorl

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"goAdapter/device"
	"goAdapter/httpServer/model"
	"goAdapter/setting"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func ApiAddInterface(context *gin.Context) {
	interfaceInfo := &struct {
		CollectInterfaceName string `json:"CollInterfaceName"` // 采集接口名字
		CommInterfaceName    string `json:"CommInterfaceName"` // 通信接口名字
		PollPeriod           int    `json:"PollPeriod"`
		OfflinePeriod        int    `json:"OfflinePeriod"`
	}{}

	err := context.ShouldBindJSON(interfaceInfo)
	if err != nil {
		fmt.Println("interfaceInfo json unMarshall err,", err)

		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: "json unMarshall err",
		})
		return
	}

	err = device.AddCollectInterface(interfaceInfo.CollectInterfaceName,
		interfaceInfo.CommInterfaceName,
		interfaceInfo.PollPeriod,
		interfaceInfo.OfflinePeriod)
	if err != nil {
		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, model.Response{
		Code: "0",
	})
}

func ApiModifyInterface(context *gin.Context) {

	aParam := model.Response{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	interfaceInfo := &struct {
		CollectInterfaceName string `json:"CollInterfaceName"` // 采集接口名字
		CommInterfaceName    string `json:"CommInterfaceName"` // 通信接口名字
		PollPeriod           int
		OfflinePeriod        int
	}{}

	err := json.Unmarshal(bodyBuf[:n], interfaceInfo)
	if err != nil {
		fmt.Println("interfaceInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		context.JSON(http.StatusOK, aParam)
		return
	}

	err = device.ModifyCollectInterface(interfaceInfo.CollectInterfaceName,
		interfaceInfo.CommInterfaceName,
		interfaceInfo.PollPeriod,
		interfaceInfo.OfflinePeriod)
	if err != nil {
		aParam.Code = "1"
		aParam.Message = err.Error()
		aParam.Data = ""
		context.JSON(http.StatusOK, aParam)
		return
	}

	aParam.Code = "0"
	aParam.Data = ""
	context.JSON(http.StatusOK, aParam)
	return

}

func ApiDeleteInterface(context *gin.Context) {

	aParam := model.Response{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	interfaceInfo := &struct {
		CollectInterfaceName string `json:"CollInterfaceName"` // 采集接口名字
	}{}

	err := json.Unmarshal(bodyBuf[:n], interfaceInfo)
	if err != nil {
		fmt.Println("interfaceInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		context.JSON(http.StatusOK, aParam)
		return
	}

	err = device.DeleteCollectInterface(interfaceInfo.CollectInterfaceName)
	if err != nil {
		aParam.Code = "1"
		aParam.Message = err.Error()
		aParam.Data = ""
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	aParam.Code = "0"
	aParam.Message = ""
	aParam.Data = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiGetInterfaceInfo(context *gin.Context) {

	//采集接口模板
	type CollectInterfaceTemplate struct {
		CollInterfaceName   string                      `json:"CollInterfaceName"`   //采集接口
		CommInterfaceName   string                      `json:"CommInterfaceName"`   //通信接口 		//通信队列
		PollPeriod          int                         `json:"PollPeriod"`          //采集周期
		OfflinePeriod       int                         `json:"OfflinePeriod"`       //离线超时周期
		DeviceNodeCnt       int                         `json:"DeviceNodeCnt"`       //设备数量
		DeviceNodeOnlineCnt int                         `json:"DeviceNodeOnlineCnt"` //设备在线数量
		DeviceNodeMap       []device.DeviceNodeTemplate `json:"DeviceNodeMap"`       //节点表
	}

	sName := context.Query("CollInterfaceName")

	aParam := &struct {
		Code    string
		Message string
		Data    CollectInterfaceTemplate
	}{}

	coll, ok := device.CollectInterfaceMap.Coll[sName]
	if !ok {
		aParam.Code = "1"
		aParam.Message = "interface is not exist"

		context.JSON(http.StatusOK, aParam)
		return
	}

	collParam := CollectInterfaceTemplate{
		CollInterfaceName:   coll.CollInterfaceName,
		CommInterfaceName:   coll.CommInterfaceName,
		PollPeriod:          coll.PollPeriod,
		OfflinePeriod:       coll.OfflinePeriod,
		DeviceNodeCnt:       coll.DeviceNodeCnt,
		DeviceNodeOnlineCnt: coll.DeviceNodeOnlineCnt,
		DeviceNodeMap:       make([]device.DeviceNodeTemplate, 0),
	}
	for _, v := range coll.DeviceNodeMap {
		collParam.DeviceNodeMap = append(collParam.DeviceNodeMap, *v)
	}

	aParam.Code = "0"
	aParam.Message = ""
	aParam.Data = collParam
	context.JSON(http.StatusOK, aParam)
}

func ApiGetAllInterfaceInfo(context *gin.Context) {

	type InterfaceParamTemplate struct {
		CollInterfaceName   string `json:"CollInterfaceName"`   // 采集接口
		CommInterfaceName   string `json:"CommInterfaceName"`   // 通信接口
		PollPeriod          int    `json:"PollPeriod"`          // 采集周期
		OfflinePeriod       int    `json:"OfflinePeriod"`       // 离线超时周期
		DeviceNodeCnt       int    `json:"DeviceNodeCnt"`       // 设备数量
		DeviceNodeOnlineCnt int    `json:"DeviceNodeOnlineCnt"` // 设备在线数量
	}

	aParam := &struct {
		Code    string
		Message string
		Data    []InterfaceParamTemplate
	}{}

	aParam.Data = make([]InterfaceParamTemplate, 0)

	aParam.Code = "0"
	aParam.Message = ""

	for _, v := range device.CollectInterfaceMap.Coll {
		Param := InterfaceParamTemplate{
			CollInterfaceName:   v.CollInterfaceName,
			CommInterfaceName:   v.CommInterfaceName,
			PollPeriod:          v.PollPeriod,
			OfflinePeriod:       v.OfflinePeriod,
			DeviceNodeCnt:       v.DeviceNodeCnt,
			DeviceNodeOnlineCnt: v.DeviceNodeOnlineCnt,
		}
		aParam.Data = append(aParam.Data, Param)
	}

	////排序，方便前端页面显示
	//sort.Slice(aParam.Data, func(i, j int) bool {
	//	iName, _ := strconv.Atoi(aParam.Data[i].CollInterfaceName)
	//	jName, _ := strconv.Atoi(aParam.Data[j].CollInterfaceName)
	//	return iName > jName
	//})

	context.JSON(http.StatusOK, aParam)
}

func ApiSendDirectDataToCollInterface(context *gin.Context) {
	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	serviceInfo := struct {
		CollInterfaceName string
		DirectData        string
	}{}
	err := json.Unmarshal(bodyBuf[:n], &serviceInfo)
	if err != nil {
		fmt.Println("serviceInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	device.CollectInterfaceMap.Lock.Lock()
	coll, ok := device.CollectInterfaceMap.Coll[serviceInfo.CollInterfaceName]
	device.CollectInterfaceMap.Lock.Unlock()
	if !ok {
		aParam.Code = "1"
		aParam.Message = "collInterface is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	//去掉字符串中的空格
	serviceInfo.DirectData = strings.ReplaceAll(serviceInfo.DirectData, " ", "")
	if len(serviceInfo.DirectData)%2 != 0 {
		aParam.Code = "1"
		aParam.Message = "16进制数据格式不正确，需要偶数个数字"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	reqData, _ := hex.DecodeString(serviceInfo.DirectData)
	setting.ZAPS.Debugf("reqData %X", reqData)
	req := device.CommunicationDirectDataReqTemplate{
		CollInterfaceName: serviceInfo.CollInterfaceName,
		Data:              reqData,
	}
	ack := coll.CommQueueManage.CommunicationManageAddDirectData(req)
	if ack.Status == true {
		aParam.Code = "0"
		ackStr := hex.EncodeToString(ack.Data)
		aParam.Data = strings.ToUpper(ackStr)
		aParam.Message = ""
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
	} else {
		aParam.Code = "1"
		aParam.Data = ""
		aParam.Message = ""
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
	}

}
