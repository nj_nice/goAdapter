package contorl

import (
	"fmt"
	"goAdapter/httpServer/model"
	"goAdapter/setting"
	"net/http"
	"sort"

	"github.com/gin-gonic/gin"
)

func ApiAddNetwork(context *gin.Context) {

	networkParam := setting.NetworkConfigParamTemplate{}

	err := context.ShouldBindJSON(&networkParam)
	if err != nil {
		setting.ZAPS.Warnf("rNetworkParam json unMarshall err,", err)
		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: "",
			Data:    "",
		})
		return
	}

	err = setting.AddNetworkConfigParam(networkParam)
	if err != nil {
		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: err.Error(),
			Data:    "",
		})
	} else {
		context.JSON(http.StatusOK, model.Response{
			Code:    "0",
			Message: "",
			Data:    "",
		})
	}
}

func ApiModifyNetwork(context *gin.Context) {

	networkParam := setting.NetworkConfigParamTemplate{}

	err := context.ShouldBindJSON(&networkParam)
	if err != nil {
		fmt.Println("rNetworkParam json unMarshall err,", err)
		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: "",
			Data:    "",
		})
		return
	}

	setting.ModifyNetworkConfigParam(networkParam)
	context.JSON(http.StatusOK, model.Response{
		Code:    "0",
		Message: "",
		Data:    "",
	})
}

func ApiDeleteNetwork(context *gin.Context) {
	type NetworkConfigParamTemplate struct {
		Name string `json:"Name"`
	}

	networkParam := NetworkConfigParamTemplate{}

	err := context.ShouldBindJSON(&networkParam)
	if err != nil {
		fmt.Println("rNetworkParam json unMarshall err,", err)
		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: "",
			Data:    "",
		})
		return
	}

	err = setting.DeleteNetworkConfigParam(networkParam.Name)
	if err != nil {
		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: err.Error(),
			Data:    "",
		})
	} else {
		context.JSON(http.StatusOK, model.Response{
			Code:    "0",
			Message: "",
			Data:    "",
		})
	}

}

func ApiGetNetwork(context *gin.Context) {

	networkParamMap := make([]setting.NetworkParamTemplate, 0)

	for _, v := range setting.NetworkParamMap.NetworkParam {
		networkParamMap = append(networkParamMap, v)
	}

	//排序，方便前端页面显示
	sort.Slice(networkParamMap, func(i, j int) bool {
		iName := networkParamMap[i].Index
		jName := networkParamMap[j].Index
		return iName < jName
	})

	context.JSON(http.StatusOK, model.ResponseData{
		Code: "0",
		Data: networkParamMap,
	})
}

func ApiGetNetworkLinkState(context *gin.Context) {

}
