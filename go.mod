module goAdapter

go 1.16

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/StackExchange/wmi v0.0.0-20210224194228-fe8f1750fd46 // indirect
	github.com/beevik/ntp v0.3.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/eclipse/paho.mqtt.golang v1.3.3
	github.com/fatih/color v1.13.0
	github.com/gin-gonic/gin v1.7.1
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/goburrow/serial v0.1.0
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/robfig/cron v1.2.0
	github.com/shirou/gopsutil/v3 v3.21.3
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/thinkgos/gomodbus/v2 v2.2.2
	github.com/yuin/gluamapper v0.0.0-20150323120927-d836955830e7
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da
	go.uber.org/zap v1.19.1
	gopkg.in/ini.v1 v1.62.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	layeh.com/gopher-luar v1.0.8
)
