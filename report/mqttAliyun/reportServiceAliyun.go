package mqttAliyun

import (
	"encoding/json"
	"fmt"
	"goAdapter/device"
	"goAdapter/device/eventBus"
	"goAdapter/setting"
	"goAdapter/utils"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/robfig/cron"
)

//阿里云上报节点参数结构体
type ReportServiceNodeParamAliyunTemplate struct {
	ServiceName       string
	CollInterfaceName string
	Name              string
	Addr              string
	CommStatus        string
	ReportErrCnt      int `json:"-"`
	ReportStatus      string
	Protocol          string
	Param             struct {
		ProductKey   string
		DeviceName   string
		DeviceSecret string
	}
}

//阿里云上报网关参数结构体
type ReportServiceGWParamAliyunTemplate struct {
	ServiceName  string
	IP           string
	Port         string
	ReportStatus string
	ReportTime   int
	ReportErrCnt int
	Protocol     string
	Param        struct {
		ProductKey   string
		DeviceName   string
		DeviceSecret string
	}
	MQTTClient MQTT.Client `json:"-"`
}

//阿里云上报服务参数，网关参数，节点参数
type ReportServiceParamAliyunTemplate struct {
	GWParam                             ReportServiceGWParamAliyunTemplate
	NodeList                            []ReportServiceNodeParamAliyunTemplate
	ReceiveFrameChan                    chan MQTTAliyunReceiveFrameTemplate               `json:"-"`
	LogInRequestFrameChan               chan []string                                     `json:"-"` //上线
	ReceiveLogInAckFrameChan            chan MQTTAliyunLogInAckTemplate                   `json:"-"`
	LogOutRequestFrameChan              chan []string                                     `json:"-"`
	ReceiveLogOutAckFrameChan           chan MQTTAliyunLogOutAckTemplate                  `json:"-"`
	ReportPropertyRequestFrameChan      chan MQTTAliyunReportPropertyTemplate             `json:"-"`
	ReceiveReportPropertyAckFrameChan   chan MQTTAliyunReportPropertyAckTemplate          `json:"-"`
	InvokeThingsServiceRequestFrameChan chan MQTTAliyunInvokeThingsServiceRequestTemplate `json:"-"`
	InvokeThingsServiceAckFrameChan     chan MQTTAliyunInvokeThingsServiceAckTemplate     `json:"-"`
}

type ReportServiceParamListAliyunTemplate struct {
	ServiceList []*ReportServiceParamAliyunTemplate
}

//实例化上报服务
var ReportServiceParamListAliyun = &ReportServiceParamListAliyunTemplate{
	ServiceList: make([]*ReportServiceParamAliyunTemplate, 0),
}

func ReportServiceAliyunInit() {

	ReportServiceParamListAliyun.ReadParamFromJson()

	//初始化
	for _, v := range ReportServiceParamListAliyun.ServiceList {
		v.ReceiveFrameChan = make(chan MQTTAliyunReceiveFrameTemplate, 100)
		v.LogInRequestFrameChan = make(chan []string, 0)
		v.ReceiveLogInAckFrameChan = make(chan MQTTAliyunLogInAckTemplate, 5)
		v.LogOutRequestFrameChan = make(chan []string, 0)
		v.ReceiveLogOutAckFrameChan = make(chan MQTTAliyunLogOutAckTemplate, 5)
		v.ReportPropertyRequestFrameChan = make(chan MQTTAliyunReportPropertyTemplate, 50)
		v.ReceiveReportPropertyAckFrameChan = make(chan MQTTAliyunReportPropertyAckTemplate, 50)
		v.InvokeThingsServiceRequestFrameChan = make(chan MQTTAliyunInvokeThingsServiceRequestTemplate, 50)
		v.InvokeThingsServiceAckFrameChan = make(chan MQTTAliyunInvokeThingsServiceAckTemplate, 50)

		go ReportServiceAliyunPoll(v)
	}
}

func fileExist(path string) bool {
	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}

func (s *ReportServiceParamListAliyunTemplate) ReadParamFromJson() bool {

	data, err := utils.FileRead("/selfpara/reportServiceParamListAliyun.json")
	if err != nil {
		setting.ZAPS.Debugf("上报服务[阿里云]配置json文件读取失败 %v", err)
		return false
	}
	err = json.Unmarshal(data, s)
	if err != nil {
		setting.ZAPS.Errorf("上报服务[阿里云]配置json文件格式化失败")
		return false
	}
	setting.ZAPS.Debugf("上报服务[阿里云]配置json文件读取成功")
	return true
}

func (s *ReportServiceParamListAliyunTemplate) WriteParamToJson() {
	utils.DirIsExist("./selfpara")
	sJson, _ := json.Marshal(*s)
	err := utils.FileWrite("/selfpara/reportServiceParamListAliyun.json", sJson)
	if err != nil {
		setting.ZAPS.Errorf("上报服务[阿里云]配置json文件写入失败")
		return
	}
	setting.ZAPS.Debugf("上报服务[阿里云]配置json文件写入成功")
}

func (s *ReportServiceParamListAliyunTemplate) AddReportService(param ReportServiceGWParamAliyunTemplate) {

	for k, v := range s.ServiceList {
		//存在相同的，表示修改;不存在表示增加
		if v.GWParam.ServiceName == param.ServiceName {

			s.ServiceList[k].GWParam = param
			s.WriteParamToJson()
			return
		}
	}

	ReportServiceParam := &ReportServiceParamAliyunTemplate{
		GWParam: param,
	}
	s.ServiceList = append(s.ServiceList, ReportServiceParam)

	s.WriteParamToJson()
}

func (s *ReportServiceParamListAliyunTemplate) DeleteReportService(serviceName string) {

	for k, v := range s.ServiceList {
		if v.GWParam.ServiceName == serviceName {

			s.ServiceList = append(s.ServiceList[:k], s.ServiceList[k+1:]...)
			s.WriteParamToJson()
			return
		}
	}
}

func (r *ReportServiceParamAliyunTemplate) AddReportNode(param ReportServiceNodeParamAliyunTemplate) {

	param.CommStatus = "offLine"
	param.ReportStatus = "offLine"
	param.ReportErrCnt = 0

	//节点存在则进行修改
	for k, v := range r.NodeList {
		//节点已经存在
		if v.Name == param.Name {
			r.NodeList[k] = param
			ReportServiceParamListAliyun.WriteParamToJson()
			return
		}
	}

	//节点不存在则新建
	r.NodeList = append(r.NodeList, param)
	ReportServiceParamListAliyun.WriteParamToJson()

	setting.ZAPS.Debugf("param %v", ReportServiceParamListAliyun)
}

func (r *ReportServiceParamAliyunTemplate) DeleteReportNode(name string) int {

	index := -1
	//节点存在则进行修改
	for k, v := range r.NodeList {
		//节点已经存在
		if v.Name == name {
			index = k
			r.NodeList = append(r.NodeList[:k], r.NodeList[k+1:]...)
			ReportServiceParamListAliyun.WriteParamToJson()
			return index
		}
	}
	return index
}

func (r *ReportServiceParamAliyunTemplate) ProcessUpLinkFrame() {

	for {
		select {
		case reqFrame := <-r.LogInRequestFrameChan:
			{
				r.LogIn(reqFrame)
			}
		case reqFrame := <-r.LogOutRequestFrameChan:
			{
				r.LogOut(reqFrame)
			}
		case reqFrame := <-r.ReportPropertyRequestFrameChan:
			{
				if reqFrame.DeviceType == "gw" {
					r.GWPropertyPost()
				} else if reqFrame.DeviceType == "node" {
					r.NodePropertyPost(reqFrame.DeviceName)
				}
			}
		}
	}
}

func (r *ReportServiceParamAliyunTemplate) ProcessDownLinkFrame() {

	for {
		select {
		case frame := <-r.ReceiveFrameChan:
			{
				setting.ZAPS.Debugf("Recv TOPIC: %s", frame.Topic)
				setting.ZAPS.Debugf("Recv MSG: %v", frame.Payload)

				if strings.Contains(frame.Topic, "/thing/event/property/pack/post_reply") { //网关、子设备上报属性回应

					ackFrame := MQTTAliyunReportPropertyAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.ZAPS.Errorf("ReportPropertyAck json unmarshal err")
						return
					}
					r.ReceiveReportPropertyAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/combine/batch_login_reply") { //子设备上线回应

					ackFrame := MQTTAliyunLogInAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.ZAPS.Warnf("LogInAck json unmarshal err")
						return
					}
					r.ReceiveLogInAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/combine/batch_logout_reply") { //子设备下线回应

					ackFrame := MQTTAliyunLogOutAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.ZAPS.Errorf("LogOutAck json unmarshal err")
						return
					}
					r.ReceiveLogOutAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/thing/service") { //设备服务调用
					serviceFrame := MQTTAliyunInvokeThingsServiceRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &serviceFrame)
					if err != nil {
						setting.ZAPS.Errorf("serviceFrame json unmarshal err")
						return
					}
					r.InvokeThingsServiceRequestFrameChan <- serviceFrame
				} else if strings.Contains(frame.Topic, "/thing/service/property/set") { //设置属性请求

				}
			}
		}
	}
}

func (r *ReportServiceParamAliyunTemplate) ProcessCollEvent(sub eventBus.Sub) {

	for {
		select {
		case msg := <-sub.Out():
			{
				subMsg := msg.(device.CollectInterfaceEventTemplate)
				//判断设备在该上报服务中
				index := -1
				for k, v := range r.NodeList {
					if v.Name == subMsg.NodeName {
						index = k
					}
				}
				if index == -1 {
					continue
				}
				setting.ZAPS.Debugf("Aliyun collEvent ReceiveMsg %v", msg)
				nodeName := make([]string, 0)
				switch subMsg.Topic {
				case "onLine":
					{
						nodeName = append(nodeName, subMsg.NodeName)
						r.NodeList[index].CommStatus = "onLine"
						r.LogInRequestFrameChan <- nodeName
					}
				case "offLine":
					{
						nodeName = append(nodeName, subMsg.NodeName)
						r.NodeList[index].CommStatus = "offLine"
						r.LogOutRequestFrameChan <- nodeName
					}
				case "update":
					{
						//先判断设备在该上报服务中
						index := -1
						for k, v := range r.NodeList {
							if v.Name == subMsg.NodeName {
								index = k
							}
						}
						if index == -1 {
							continue
						}

						nodeName := make([]string, 0)
						coll, ok := device.CollectInterfaceMap.Coll[subMsg.CollName]
						if !ok {
							continue
						}
						node, ok := coll.DeviceNodeMap[subMsg.NodeName]
						if !ok {
							continue
						}

						reportStatus := false
						for _, v := range node.Properties {
							if v.Params.StepAlarm == true {
								valueCnt := len(v.Value)
								if valueCnt >= 2 { //阶跃报警必须是2个值
									if v.Type == device.PropertyTypeInt32 {
										pValueCur := v.Value[valueCnt-1].Value.(int32)
										pValuePre := v.Value[valueCnt-2].Value.(int32)
										step, _ := strconv.Atoi(v.Params.Step)
										if math.Abs(float64(pValueCur-pValuePre)) > float64(step) {
											reportStatus = true //满足报警条件，上报
											nodeName = append(nodeName, node.Name)
										}
									} else if v.Type == device.PropertyTypeUInt32 {
										pValueCur := v.Value[valueCnt-1].Value.(uint32)
										pValuePre := v.Value[valueCnt-2].Value.(uint32)
										step, _ := strconv.Atoi(v.Params.Step)
										if math.Abs(float64(pValueCur-pValuePre)) > float64(step) {
											reportStatus = true //满足报警条件，上报
											nodeName = append(nodeName, node.Name)
										}
									} else if v.Type == device.PropertyTypeDouble {
										pValueCur := v.Value[valueCnt-1].Value.(float64)
										pValuePre := v.Value[valueCnt-2].Value.(float64)
										step, err := strconv.ParseFloat(v.Params.Step, 64)
										if err != nil {
											continue
										}
										if math.Abs(pValueCur-pValuePre) > step {
											reportStatus = true //满足报警条件，上报
											nodeName = append(nodeName, node.Name)
										}
									}
								}
							} else if v.Params.MinMaxAlarm == true {
								valueCnt := len(v.Value)
								if v.Type == device.PropertyTypeInt32 {
									pValueCur := v.Value[valueCnt-1].Value.(int32)
									min, _ := strconv.Atoi(v.Params.Min)
									max, _ := strconv.Atoi(v.Params.Max)
									if pValueCur < int32(min) || pValueCur > int32(max) {
										reportStatus = true //满足报警条件，上报
										nodeName = append(nodeName, node.Name)
									}
								} else if v.Type == device.PropertyTypeUInt32 {
									pValueCur := v.Value[valueCnt-1].Value.(uint32)
									min, _ := strconv.Atoi(v.Params.Min)
									max, _ := strconv.Atoi(v.Params.Max)
									if pValueCur < uint32(min) || pValueCur > uint32(max) {
										reportStatus = true //满足报警条件，上报
										nodeName = append(nodeName, node.Name)
									}
								} else if v.Type == device.PropertyTypeDouble {
									pValueCur := v.Value[valueCnt-1].Value.(float64)
									min, err := strconv.ParseFloat(v.Params.Min, 64)
									if err != nil {
										continue
									}
									max, err := strconv.ParseFloat(v.Params.Max, 64)
									if err != nil {
										continue
									}
									if pValueCur < min || pValueCur > max {
										reportStatus = true //满足报警条件，上报
										nodeName = append(nodeName, node.Name)
									}
								}
							}
						}

						if reportStatus == true {
							reportNodeProperty := MQTTAliyunReportPropertyTemplate{
								DeviceType: "node",
								DeviceName: nodeName,
							}
							r.ReportPropertyRequestFrameChan <- reportNodeProperty
						}
					}
				}
			}
		}
	}
}

func (r *ReportServiceParamAliyunTemplate) LogIn(nodeName []string) {

	//清空接收chan，避免出现有上次接收的缓存
	for i := 0; i < len(r.ReceiveLogInAckFrameChan); i++ {
		<-r.ReceiveLogInAckFrameChan
	}

	r.NodeLogin(nodeName)
}

func (r *ReportServiceParamAliyunTemplate) LogOut(nodeName []string) {

	//清空接收chan，避免出现有上次接收的缓存
	for i := 0; i < len(r.ReceiveLogOutAckFrameChan); i++ {
		<-r.ReceiveLogOutAckFrameChan
	}

	r.NodeLogOut(nodeName)
}

func (r *ReportServiceParamAliyunTemplate) ReportTimeFun() {

	if r.GWParam.ReportStatus == "onLine" {
		//网关上报
		reportGWProperty := MQTTAliyunReportPropertyTemplate{
			DeviceType: "gw",
		}
		r.ReportPropertyRequestFrameChan <- reportGWProperty

		//全部末端设备上报
		nodeName := make([]string, 0)
		for _, v := range r.NodeList {
			nodeName = append(nodeName, v.Name)
		}
		setting.ZAPS.Debugf("report Nodes %v", nodeName)
		if len(nodeName) > 0 {
			reportNodeProperty := MQTTAliyunReportPropertyTemplate{
				DeviceType: "node",
				DeviceName: nodeName,
			}
			r.ReportPropertyRequestFrameChan <- reportNodeProperty
		}
	}
}

//查看上报服务中设备是否离线
func (r *ReportServiceParamAliyunTemplate) ReportOfflineTimeFun() {

	setting.ZAPS.Infof("service:%s CheckReportOffline", r.GWParam.ServiceName)
	if r.GWParam.ReportErrCnt >= 3 {
		r.GWParam.ReportStatus = "offLine"
		r.GWParam.ReportErrCnt = 0
		setting.ZAPS.Warnf("service:%s gw offline", r.GWParam.ServiceName)
	}

	for k, v := range r.NodeList {
		if v.ReportErrCnt >= 3 {
			r.NodeList[k].ReportStatus = "offLine"
			r.NodeList[k].ReportErrCnt = 0
			setting.ZAPS.Warnf("service:%s %s offline", v.ServiceName, v.Name)
		}
	}
}

func ReportServiceAliyunPoll(r *ReportServiceParamAliyunTemplate) {

	reportState := 0

	// 定义一个cron运行器
	cronProcess := cron.New()

	reportTime := fmt.Sprintf("@every %dm%ds", r.GWParam.ReportTime/60, r.GWParam.ReportTime%60)
	setting.ZAPS.Infof("reportServiceAliyun reportTime%v", reportTime)

	reportOfflineTime := fmt.Sprintf("@every %dm%ds", (3*r.GWParam.ReportTime)/60, (3*r.GWParam.ReportTime)%60)
	setting.ZAPS.Infof("reportServiceAliyun reportOfflineTime%v", reportOfflineTime)

	_ = cronProcess.AddFunc(reportOfflineTime, r.ReportOfflineTimeFun)
	_ = cronProcess.AddFunc(reportTime, r.ReportTimeFun)

	cronProcess.Start()
	defer cronProcess.Stop()

	//订阅采集接口消息
	device.CollectInterfaceMap.Lock.Lock()
	for _, coll := range device.CollectInterfaceMap.Coll {
		sub := eventBus.NewSub()
		coll.CollEventBus.Subscribe("onLine", sub)
		coll.CollEventBus.Subscribe("offLine", sub)
		coll.CollEventBus.Subscribe("update", sub)
		go r.ProcessCollEvent(sub)
	}
	device.CollectInterfaceMap.Lock.Unlock()

	go r.ProcessUpLinkFrame()

	go r.ProcessDownLinkFrame()

	go r.ProcessInvokeThingsService()

	for {
		switch reportState {
		case 0:
			{
				if r.GWLogin() == true {
					reportState = 1

				} else {
					time.Sleep(5 * time.Second)
				}
			}
		case 1:
			{
				//网关
				if r.GWParam.ReportStatus == "offLine" {
					reportState = 0
					r.GWParam.ReportErrCnt = 0
				}
			}
		}

		time.Sleep(100 * time.Millisecond)
	}
}
